package me.lee.restapi.products;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lee
 *
 */
@Controller
// 링크를 사용하기 위해서 선언
@ExposesResourceFor(ProductController.class)
@RequestMapping(value = "/api/products", produces = MediaTypes.HAL_JSON_VALUE)
public class ProductController {

	// 의존성 주입 같은경우는
	// 요즘엔 생성자나 셋터? 로 쓴다고들 해서 생성자로 구현
	private final ProductRespository productRespository;
	private final ModelMapper modelMapper;
	private final ProductValidator productValidator;
	
	public ProductController(ProductRespository productRespository, ModelMapper modelMapper, ProductValidator productValidator) {
		this.productRespository = productRespository;
		this.modelMapper = modelMapper;
		this.productValidator = productValidator;
	}
	
	// get member -> 조회
	// post member/{id} - > 삽입
	// put member/{id} 수정
	
	// 스프링 3에서 추가된 URI에 변수 넣는기능
	// @PathVariable long productId 로 받을 수 있음
	// "/{productId}"
	
	@PostMapping
	public ResponseEntity createProduct(@RequestBody @Valid ProductDto productDto, Errors errors) {
		
		// 오류 검증 추가 @NotEmpty 등 javax 검증에 대한 오류가 있을 경우
		if(errors.hasErrors()) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		// validator 클래스 통한 검증
		productValidator.validate(productDto, errors);
		if(errors.hasErrors()) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		// DTO 객체를 실제 VO 객체로 변환
		Product product = modelMapper.map(productDto, Product.class);
		
		// 실제 저장 프로세서
		
		// 테스트기반으로 많이하잖아
		// 스프링독스가 TDD로 하면
		// 만들기가 쉬워
		
		// 실제 받아온 값을 디비에 저장
		Product newProduct = this.productRespository.save(product);
		// 여기까지가 우리가 보통 만든 웹인거지

		// request에 관한게 아니라
		// 응답 메세지를 만드는거야 지금
		// http://localhost/프젝명/product/1 
		URI createUri = linkTo(ProductController.class).slash(newProduct.getId()).toUri();
		
		// 제품 리소스 생성 
		ProductResource productResource = new ProductResource(product);
		
		// Resource Link 추가
		productResource.add(linkTo(ProductController.class).withRel("query-products"));
		productResource.add(linkTo(ProductController.class).slash(newProduct.getId()).withRel("update-product"));
		productResource.add(new Link("/docs/index.html#resources-products-create").withRel("profile"));
		return ResponseEntity.created(createUri).body(productResource);
	}

	
}
