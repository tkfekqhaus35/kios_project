package me.lee.restapi.products;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// json 어노테이션으로 제외할 값을 설정도 가능하지만..
// 어노테이션이 너무 남발하기 때문에 분리로 구현
// 받아와야할 데이터만 정의..
// id나 status는 받아도 상관없도록 동작함
@Builder @NoArgsConstructor @AllArgsConstructor
@Data
public class ProductDto {

	@NotEmpty
	private String name;
	@NotEmpty
	private String description;
	@Min(0)
	private int price;
	@Min(0)
	private int discountPrice;
}
