package me.lee.restapi.products;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author lee
 *
 */

// jap 조인도하고 연관관계라고 표현을 하는데
@Builder @NoArgsConstructor @AllArgsConstructor
@Getter @Setter @EqualsAndHashCode(of = "id")
// jpa에서 객체로 선언
@Entity
public class Product {
	
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	private String name;
	private String description;
	private int price;
	private int discountPrice;
	@CreationTimestamp
	private LocalDateTime registProductDateTime ;
	@Enumerated(EnumType.STRING)
	private ProductStatus productStatus = ProductStatus.USE;
	
}
