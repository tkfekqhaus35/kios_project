package me.lee.restapi.products;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

// 리소스 생성... ResourcesSupport 에서 명칭 변경
// 참조 linkg https://docs.spring.io/spring-hateoas/docs/current/reference/html/
/**
 * @author lee
 *
 */
public class ProductResource extends RepresentationModel {

	// 빈시리얼라이저가 필드명을 기준으로 래핑을 하기 때문에
	// 래핑을 제외시키는 어노테이션
	@JsonUnwrapped
	private Product product;

	public ProductResource(Product product) {
		this.product = product;
		add(linkTo(ProductController.class).slash(product.getId()).withSelfRel());
	}

	public Product getProduct() {
		return product;
	}

}
