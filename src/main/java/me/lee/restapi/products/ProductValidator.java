package me.lee.restapi.products;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;


/**
 * @author lee
 *
 */
// 검증 내용 작성
@Component
public class ProductValidator {
	public void validate(ProductDto productDto, Errors errors) {
		// errors.rejectValue("field") 는 필드에러
		// errors.reject() 는 글로벌 에러 
		if(productDto.getDiscountPrice() > productDto.getPrice()) {
			errors.rejectValue("discountPrice", "wrongValue", "price somtion wrong..");
		}
	}
}
