package me.lee.restapi.products;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lee
 *
 */
public interface ProductRespository extends JpaRepository<Product, Integer> {
}
