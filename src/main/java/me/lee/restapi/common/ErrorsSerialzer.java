package me.lee.restapi.common;

import java.io.IOException;
import java.util.List;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author LeeJuHuyk
 * @Description 에러 내용을 Json으로 생성해주는 클래스
 * @version 1.0.0
 */
// ObjectMapper에 자동으로 등록해주는 Json 어노테이션
@JsonComponent
public class ErrorsSerialzer extends JsonSerializer<Errors> {
	/**
	 * @author LeeJuHyuk
	 */
	@Override
	public void serialize(Errors value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		
		// JsonGenerator 사용 시작 메소드
		gen.writeStartArray();
		
		// 필드에러 등록
		List<FieldError> errorList = value.getFieldErrors();

		for (FieldError error : errorList) {
			gen.writeStartObject();
			gen.writeStringField("field", error.getField());
			gen.writeStringField("objectName", error.getObjectName());
			gen.writeStringField("code", error.getCode());
			gen.writeStringField("defaultMessage", error.getDefaultMessage());

			Object rejectedValue = error.getRejectedValue();

			if (rejectedValue != null) {
				gen.writeStringField("rejectedValue", rejectedValue.toString());
			}
			gen.writeEndObject();
		}

		// 글로벌 에러 등록
		List<ObjectError> globalErrorList = value.getGlobalErrors();
		for (ObjectError error : globalErrorList) {
			gen.writeStartObject();
			gen.writeStringField("objectName", error.getObjectName());
			gen.writeStringField("code", error.getCode());
			gen.writeStringField("defaultMessage", error.getDefaultMessage());
			gen.writeEndObject();
		}

		// JsonGenerator 종료 메소드
		gen.writeEndArray();

	}
}
