package me.lee.restapi.products;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import static org.springframework.restdocs.headers.HeaderDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import me.lee.restapi.common.RestDocsConfiguration;
import me.lee.restapi.common.TestDescription;
@RunWith(SpringRunner.class)
@SpringBootTest 
@AutoConfigureMockMvc
@AutoConfigureRestDocs
@Import(RestDocsConfiguration.class)
public class ProductControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;

	@Test
	@TestDescription("정상적인 제품 생성 테스트")
	public void createProducts() throws Exception {
		// perform이 request를 해주는 메소드
		ProductDto product = ProductDto.builder()
				.name("Computer")
				.price(1000000)
				.description("this is computer")
				.build();

		mockMvc.perform(post("/api/products")
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaTypes.HAL_JSON)
					.content(objectMapper.writeValueAsString(product)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(jsonPath("id").exists())
				.andExpect(header().exists(HttpHeaders.LOCATION))
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE,MediaTypes.HAL_JSON_VALUE))
				//.andExpect(jsonPath("id").value(Matchers.not(new Integer(100))))
				.andExpect(jsonPath("productStatus").value(ProductStatus.USE.name()))
				.andExpect(jsonPath("_links.self").exists())
				.andExpect(jsonPath("_links.query-products").exists())
				.andExpect(jsonPath("_links.update-product").exists())
				.andExpect(jsonPath("_links.profile").exists())
				// restDocs용 snippet 생성
				// 요청 본문, 요청응답, 링크, 요청헤더, 요청필드, 응답헤드, 응답 필드를 문서화
				// 그러나.. 포맷팅이 안된 값으로 적용되 때문에 common 패키지에 RestDocsConfiguration 생성
				// 해당 클래스를 통해 용청 본문 / 요청 응답 문서화를 완료
				.andDo(document("create-product",
						links(
								linkWithRel("self").description("link to self"),
								linkWithRel("query-products").description("link to query-product"),
								linkWithRel("update-product").description("link to update-product"),
								linkWithRel("profile").description("link to profile(index.doc)")
						),
						requestHeaders(
								headerWithName(HttpHeaders.ACCEPT).description("accept header"),
								headerWithName(HttpHeaders.CONTENT_TYPE).description("content type header")
						),
						requestFields(
								fieldWithPath("name").description("Name of new Product"),
								fieldWithPath("description").description("description of new Product"),
								fieldWithPath("price").description("price of new Product"),
								fieldWithPath("discountPrice").description("discountPrice of new Product")
						),
						responseHeaders(
								headerWithName(HttpHeaders.LOCATION).description("location header"),
								headerWithName(HttpHeaders.CONTENT_TYPE).description("content type header")
						),
						relaxedResponseFields(
								fieldWithPath("id").description("id of new Product"),
								fieldWithPath("name").description("Name of new Product"),
								fieldWithPath("description").description("description of new Product"),
								fieldWithPath("price").description("price of new Product"),
								fieldWithPath("discountPrice").description("discountPrice of new Product"),
								fieldWithPath("productStatus").description("productStatus of new Product"),
								fieldWithPath("registProductDateTime").description("registProductDateTime of new Product"),
								fieldWithPath("_links.self.href").description("link to self"),
								fieldWithPath("_links.query-products.href").description("link to query-product"),
								fieldWithPath("_links.update-product.href").description("link to update-product"),
								fieldWithPath("_links.profile.href").description("link to profile")
						)
						// 현재 registProductDateTime 오류인데 이유 찾아보기..
					))
				;
	}
	
	@Test
	@TestDescription("잘못된 요청에 대한 응답 테스트")
	public void createProductsBadRequest() throws Exception {

		Product product = Product.builder()
				.id(100)
				.name("Computer")
				.price(1000000)
				.description("this is computer")
				.productStatus(ProductStatus.DELETE)
				.build();

		mockMvc.perform(post("/api/products")
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaTypes.HAL_JSON)
					.content(objectMapper.writeValueAsString(product)))
				.andDo(print())
				.andExpect(status().isBadRequest()) 
				;
	}
	
	@Test
	@TestDescription("빈 객체 저장에 대한 응답테스트")
	public void createProductBadRequestEmptyInput() throws JsonProcessingException, Exception {
		ProductDto productDto = ProductDto.builder().build();
		
		this.mockMvc.perform(post("/api/products")
					.contentType(MediaTypes.HAL_JSON_VALUE)
					.content(this.objectMapper.writeValueAsString(productDto))
				).andExpect(status().isBadRequest());
	}
	
	@Test
	@TestDescription("잘못된 값이 입력 된 경우의 응답처리")
	public void createProductBadRequestWrongInput() throws JsonProcessingException, Exception {
		ProductDto productDto = ProductDto.builder().build();
		
		Product product = Product.builder()
				.id(100)
				.name("Computer")
				.price(1000000)
				.discountPrice(2000000)
				.description("this is computer")
				.productStatus(ProductStatus.DELETE)
				.build();
		
		this.mockMvc.perform(post("/api/products")
					.contentType(MediaTypes.HAL_JSON_VALUE)
					.content(this.objectMapper.writeValueAsString(productDto))
				)
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$[0].objectName").exists())
				.andExpect(jsonPath("$[0].defaultMessage").exists())
				.andExpect(jsonPath("$[0].code").exists())
		;
	}
	
}
