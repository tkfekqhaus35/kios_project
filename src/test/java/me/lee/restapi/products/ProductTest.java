package me.lee.restapi.products;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import me.lee.restapi.common.TestDescription;

public class ProductTest {
	
	@Test
	@TestDescription("Product 도메인 생성 테스트 ( 빌더 )")
	public void builder() {
		Product product = Product.builder()
				.name("Computer")
				.price(1000000)
				.description("보급형 컴퓨터입니다.")
				.build();
		assertThat(product).isNotNull();
	}
	
	@Test
	@TestDescription("Product 도메인 생성 테스트 ( 자바빈 스펙 )")
	// getter 있어야 작동하는 api
	public void javaBean() {
		Product product = new Product();
			
		String name = "Computer";
		int price = 1000000;
		String description = "보급형 컴퓨터입니다.";
		
		product.setName(name);
		product.setPrice(price);
		product.setDescription(description);
		
		assertThat(product.getName()).isEqualTo(name);
		assertThat(product.getPrice()).isEqualTo(price);
		assertThat(product.getDescription()).isEqualTo(description);
		
	}
}
