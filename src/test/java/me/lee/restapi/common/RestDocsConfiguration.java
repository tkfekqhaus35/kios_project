package me.lee.restapi.common;

import org.springframework.boot.test.autoconfigure.restdocs.RestDocsMockMvcConfigurationCustomizer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;

import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;

// 테스트에만 사용하는 설정이라고 알려주는 어노테이션
@TestConfiguration
public class RestDocsConfiguration {

	// 테스트라기 보다는
	// 스프링 독스를 쓰기위한 설정? 공통 설정을 해주는 클래스

	// 클래스 어노테이션에 의해 테스트에만 사용되는 빈으로 등록
	@Bean
	public RestDocsMockMvcConfigurationCustomizer configurationCustomizer() {
		return new RestDocsMockMvcConfigurationCustomizer() {

			@Override
			public void customize(MockMvcRestDocumentationConfigurer configurer) {
				configurer.operationPreprocessors()
						.withRequestDefaults(prettyPrint())
						.withResponseDefaults(prettyPrint());
			}
		};
	}
}
